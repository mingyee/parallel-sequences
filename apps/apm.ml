
open Util
open Future

type profile = {
  firstname : string;
  lastname : string;
  sex : string;
  age : int;
  lo_agepref : int;
  hi_agepref : int;
  profession : string;
  has_children : bool;
  wants_children : bool;
  leisure : string;
  drinks : bool;
  smokes : bool;
  music : string;
  orientation : string;
  build : string;
  height : string
}

let convert (p : string) : profile =
  let s = String.concat " " (Str.split (Str.regexp_string "@") p) in
  Scanf.sscanf s "%s@ %s@ %s@ %d %d %d %s@ %B %B %s@ %B %B %s@ %s@ %s@ %s"
  (fun firstname lastname sex age lo_agepref hi_agepref profession has_children
       wants_children leisure drinks smokes music orientation build height ->
   { firstname = firstname;
     lastname = lastname;
     sex = sex;
     age = age;
     lo_agepref = lo_agepref;
     hi_agepref = hi_agepref;
     profession = profession;
     has_children = has_children;
     wants_children = wants_children;
     leisure = leisure;
     drinks = drinks;
     smokes = smokes;
     music = music;
     orientation = orientation;
     build = build;
     height = height
   })

let print_profile ({
     firstname = firstname;
     lastname = lastname;
     sex = sex;
     age = age;
     lo_agepref = lo_agepref;
     hi_agepref = hi_agepref;
     profession = profession;
     has_children = has_children;
     wants_children = wants_children;
     leisure = leisure;
     drinks = drinks;
     smokes = smokes;
     music = music;
     orientation = orientation;
     build = build;
     height = height } : profile) : unit =
  Printf.printf "%s %s\n" firstname lastname;
  Printf.printf "  sex: %s  age: %d  profession: %s\n" sex age profession;
  Printf.printf "  %s  %s\n" (if drinks then "social drinker" else "nondrinker") (if smokes then "smoker" else "nonsmoker");
  Printf.printf "  %s  %s\n"
    (if has_children then "has children" else "no children")
    (if wants_children then "wants children" else "does not want children");
  Printf.printf "  prefers a %s partner between the ages of %d and %d\n"
    (if (orientation="straight" && sex="F") || (orientation = "gay/lesbian" && sex="M") then "male" else "female")
    lo_agepref hi_agepref;
  Printf.printf "  likes %s music and %s\n" music leisure


let print_matches (n : string) ((p, ps) : profile * (float * profile) list) : unit =
  print_string "------------------------------\nClient: ";
  print_profile p;
  Printf.printf "\n%s best matches:\n" n;
  List.iter (fun (bci, profile) ->
    Printf.printf "------------------------------\nCompatibility index: %f\n" bci; print_profile profile) ps;
  print_endline ""



(*module PSeq = Sequence.Seq(PFuture)(struct let use_mpi = true end)*)
module PSeq = Sequence.ListSeq
module CSet = Set.Make(struct
  type t = float * profile
  let compare ((x, xp) : t) ((y, yp) : t) : int =
    if      x > y then 1
    else if x < y then -1
    else let c = String.compare yp.lastname xp.lastname in (
      match c with
      | 0 -> String.compare yp.firstname xp.firstname
      | _ -> c
    )
  ;;
end)

(* apm computes the potential love of your life.  The filename of a file
 * containing candidate profiles is in location 0 of the given array.
 * The number of matches desired is in location 1.  The first and last name
 * of the (human) client are in location 2 and 3, respectively.  The client's
 * profile must be in the profiles file.  Results are output to stdout. *)
let matchme (args : string array) : unit =
  (* Data extraction *)
  let contents  = read_whole_file args.(0) in
  let cont_list = Str.split (Str.regexp "\n") contents in
  let prof_list = List.map convert cont_list in
  let rec find_prof prof_list first last =
    match prof_list with
    | []       -> raise (Failure "Unexpected condition")
    | hd :: tl -> (
        match String.compare hd.firstname first,
              String.compare hd.lastname last with
        | 0, 0 -> hd
        | _, _ -> find_prof tl first last
      )
  in
  let prof = find_prof prof_list args.(2) args.(3) in
  let num_matches = int_of_string args.(1) in
  
  (* Map and reduce functions *)
  let matcher (curr_prof : profile) : CSet.t (* singleton *) = (
    match String.compare curr_prof.firstname prof.firstname,
          String.compare curr_prof.lastname prof.lastname with
    | 0, 0 -> CSet.empty
    | _, _ -> (let c = 
        let a = String.compare curr_prof.orientation prof.orientation in 
        let b = String.compare curr_prof.orientation "straight" in
        let s = String.compare curr_prof.sex prof.sex in
        if a != 0 then 0.
        else if b == 0 && s == 0 then 0. 
        else if b != 0 && s != 0 then 0. 
        else if curr_prof.age < prof.lo_agepref then 0. 
        else if curr_prof.age > prof.hi_agepref then 0.
        else 1. in 
        let p = 
          if String.compare curr_prof.profession prof.profession == 0 then 1.0
          else 0.
        in
        let l = 
          if String.compare curr_prof.leisure prof.leisure == 0 then 1.0
          else 0.
        in
        let m = 
          if String.compare curr_prof.music prof.music == 0 then 1.
          else 0.
        in
        let b = 
          if String.compare curr_prof.build prof.build == 0 then 1.
          else 0.
        in
        let h = 
          if String.compare curr_prof.height prof.height == 0 then 1.0
          else 0.
        in
        let k =
          if (curr_prof.has_children == prof.wants_children) then 1.
          else 0.
        in
        let d = 
          if curr_prof.drinks == prof.drinks then 1.
          else 0.
        in
        let s =
          if curr_prof.smokes == prof.smokes then 1.
          else 0.
        in
        let f = (c *.(p +. l +. m +. b +. h +. k +. d +. s) /. 8.0) in
        CSet.singleton (f, curr_prof)
	  )
  ) in
  let top_n (set1 : CSet.t) (set2 : CSet.t) = (
    let u = CSet.union set1 set2 in
    let rec remove_n (set : CSet.t) (n : int) = (
      if n <= 0 then set
      else remove_n (CSet.remove (CSet.min_elt set) set) (n - 1)
    ) in remove_n u ((CSet.cardinal u) - num_matches)
  ) in
  
  (* Match calculation *)
  let prof_seq = PSeq.seq_of_array @@ Array.of_list @@ prof_list in
  let matches  = PSeq.map_reduce matcher top_n CSet.empty prof_seq in
  let _ = print_matches args.(1) (prof, List.rev @@ CSet.elements matches) in
  ()
;;
