open Future
open Util

(*module PSeq = Sequence.Seq(PFuture)(struct let use_mpi = true end)*)
module PSeq = Sequence.ListSeq

module TitleSet = Set.Make(String)

module WordMap = Map.Make(String)

let set_to_list set = 
  TitleSet.fold (fun elt l -> elt :: l) set []

let map_to_list map =
  WordMap.fold (fun key a b -> (key, a) :: b) map []

(* inverted_index computes an inverted index for the contents of
 * a given file. The filename is the given string.
 * The results are output to stdout. *)
let mkindex (args : string ) : unit =  
  let file = PSeq.seq_of_array (Array.of_list (load_documents args)) in
  let docseq = PSeq.map (fun d -> (d.title, split_words d.contents)) file in
  let m (title, wordlist) = (
    List.fold_left (fun wmap w ->
        WordMap.add (String.lowercase w) (TitleSet.singleton title) wmap
      ) WordMap.empty wordlist
  ) in
  let ins (k : string) v1 map = (
    match
      try Some (WordMap.find k map) with
      | Not_found -> None 
    with
    | None    -> WordMap.add k v1 map
    | Some v2 -> WordMap.add k (TitleSet.union v1 v2) map
  ) in
  let r b a = (
    if (WordMap.cardinal a) > (WordMap.cardinal b) then WordMap.fold ins b a
    else WordMap.fold ins a b
  ) in
  let index = PSeq.map_reduce m r WordMap.empty docseq in
  let lindex = WordMap.map set_to_list index in
  let findex = map_to_list lindex in
  print_reduce_results findex
;;
