open System
open Future 
open Mpi



module type S = sig
  type 'a t
  val tabulate : (int -> 'a) -> int -> 'a t
  val seq_of_array : 'a array -> 'a t
  val array_of_seq : 'a t -> 'a array
  val iter: ('a -> unit) -> 'a t -> unit
  val length : 'a t -> int
  val empty : unit  ->'a t
  val cons : 'a -> 'a t -> 'a t
  val singleton : 'a -> 'a t
  val append : 'a t -> 'a t -> 'a t
  val nth : 'a t -> int -> 'a
  val map : ('a -> 'b) -> 'a t -> 'b t
  val map_reduce : ('a -> 'b) -> ('b -> 'b -> 'b) -> 'b -> 'a t -> 'b
  val reduce : ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a
  val flatten : 'a t t -> 'a t
  val repeat : 'a -> int -> 'a t
  val zip : ('a t * 'b t) -> ('a * 'b) t
  val split : 'a t -> int -> 'a t * 'a t
  val scan: ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a t
end



(*******************************************************)
(* Sequential Sequences Based on a List Representation *)
(*******************************************************)

module ListSeq : S = struct

  type 'a t = 'a list

  let length = List.length

  let empty () = []

  let cons (x:'a) (s:'a t) = x::s

  let singleton x = [x]

  let append = List.append

  let tabulate f n =
    let rec helper acc x =
      if x = n then List.rev acc
      else helper ((f x)::acc) (x+1) in
    helper [] 0

  let nth = List.nth

  let filter = List.filter

  let map = List.map

  let reduce = List.fold_left

  let map_reduce m r b s = reduce r b (map m s)

  let repeat x n =
    let rec helper x n acc =
      if n = 0 then acc else helper x (n-1) (x::acc) in
    helper x n []

  let flatten = List.flatten

  let zip (s1,s2) = List.combine s1 s2

  let split s i =
    let rec helper s i acc =
      match s,i with
        | [],_ -> failwith "split"
        | _,0 -> (List.rev acc,s)
        | h::t,_ -> helper t (i-1) (h::acc) in
    helper s i []

  let iter = List.iter

  let array_of_seq = Array.of_list

  let seq_of_array = Array.to_list

  let scan f b s = 
    let (_,xs) = List.fold_left (fun (v,ls) e -> let r = f v e in (r,r::ls)) (b,[]) s in
    List.rev xs

end


(*******************************************************)
(* Parallel Sequences                                  *)
(*******************************************************)

module type SEQ_ARGS = sig 
  val use_mpi: bool
end


module Seq (Par : Future.S) (Arg : SEQ_ARGS) : S = struct
  
  type 'a t = 'a array
  
  let num_cores = System.cpu_count ()
  
  let chunk_n len = 
    let mult_const = 3 in
    min (num_cores * mult_const) (max (len / 8) 1)

  let rec split_array a n c out =
    if n <= 0 then ()
    else
      let rem_len = (Array.length a - c) in
      let q, r = rem_len / n, rem_len mod n in
      let len = if r > 0 then q + 1 else q in
      out.((chunk_n @@ Array.length @@ a)-n) <- Array.sub a c len;
      split_array a (n-1) (c+len) out
  
  let seq_of_array a = a
  
  let array_of_seq seq = seq
  
  let iter f seq = Array.iter f seq
  
  let length seq = Array.length seq
  
  let empty () = [||]
  
  let cons elem seq = Array.append [|elem|] seq
  
  let singleton elem = [|elem|]
  
  let append seq1 seq2 = Array.append seq1 seq2
  
  let nth seq i = seq.(i)
  
  let flatten seqseq = Array.fold_right Array.append seqseq [||]
  
  let map f seq =
    let chunks = Array.make (chunk_n @@ Array.length @@ seq) [||] in
    split_array seq (chunk_n @@ Array.length @@ seq) 0 chunks;
    let chunk_to_future x = Par.future (Array.map f) x in
    let futures = Array.map chunk_to_future chunks in
    flatten (Array.map Par.force futures)
  
  let chunk_r r a =
    let (hd, tl) = (a.(0), Array.sub a 1 (Array.length a - 1)) in
    Array.fold_left r hd tl
  
  let map_reduce m r b seq =
    if (Array.length seq == 0) then b
    else
      let chunks = Array.make (chunk_n @@ Array.length @@ seq) [||] in
      split_array seq (chunk_n @@ Array.length @@ seq) 0 chunks;
      let futures = Array.map
        (Par.future (fun a -> chunk_r r (Array.map m a)))
        chunks
      in Array.fold_left r b (Array.map Par.force futures)
  
  let reduce r b seq =
    if (Array.length seq == 0) then b
    else 
      let chunks = Array.make (chunk_n @@ Array.length @@ seq) [||] in
      split_array seq (chunk_n @@ Array.length @@ seq) 0 chunks;
      let chunk_to_future x = Par.future (chunk_r r) x in
      let futures = Array.map chunk_to_future chunks in
      Array.fold_left r b (Array.map Par.force futures)
  
  let repeat elem num = Array.make num elem
  
  let zip (seq1, seq2) = 
    let len = min (Array.length seq1) (Array.length seq2) in
    if len = 0 then [||]
    else
      let zipped = Array.make len (seq1.(0), seq2.(0)) in
      Array.iteri (fun i x -> zipped.(i) <- (seq1.(i), seq2.(i))) zipped;
      zipped
  
  let split seq x = 
    if Array.length seq = 0 then failwith "msg"
    else (Array.sub seq 0 x, Array.sub seq x ((Array.length seq) - x))
  
  let tabulate f n = 
    let seq = Array.make (chunk_n n) [||] in
    split_array (Array.init n (fun x -> x)) (chunk_n n) 0 seq;
    let futures = Array.map (Par.future (Array.map f)) seq in
    flatten (Array.map Par.force futures)
  
  (*******************************************************)
  (* Parallel Prefix Sum                                 *)
  (*******************************************************)
type 'a tree =
  | Leaf
  | Node of int * 'a * 'a tree * 'a tree

type 'a compute_message =
  | CSum of 'a t
  | Left of 'a
        
type 'a result_message =
  | RSum of 'a
  | Result of 'a t 
(*TODO: use_mpi bool not used*)
  (* Here you will implement a version of the parallel prefix scan for a sequence 
   * [a0; a1; ...], the result of scan will be [f base a0; f (f base a0) a1; ...] *)
  let scan (f: 'a -> 'a -> 'a) (base: 'a) (seq: 'a t) : 'a t =
    if not Arg.use_mpi then
      let (_, xs) = Array.fold_left (fun (x, tl) e -> 
                        let r = f x e in  (r, r :: tl)) (base, []) seq
      in
      Array.of_list @@ List.rev xs
      
      
    else (
      if Array.length seq = 0 then [||]
      else
        let pps_up (a : 'a t) : 'a tree = (
          let bottom = Array.mapi (fun i elt -> Node (i, elt, Leaf, Leaf)) a in
          let rec level (ls : 'a tree list) = (
            let rec row (ls : 'a tree list) (next : 'a tree list) = (
              match ls with 
              | hd1 :: hd2 :: tl -> (
                  match hd1, hd2 with
                  | Node (_, x, _, _), Node (_, y, _, _) ->             
                      row tl (Node (-1, f x y, hd1, hd2) :: next)
                  | _, _ -> raise (Failure "Unexpected match condition.")
                )
              | [hd]             -> List.rev (hd :: next)
              | []               -> List.rev next
            ) in
            match row ls [] with
            | []   -> raise (Failure "unexpected empty list")
            | [hd] -> hd
            | ls   -> level ls
          ) in
          level (Array.to_list bottom)
        ) in
        let pps_down (tree : 'a tree) (fl : 'a) (n : int) : 'a array =
          let out = Array.make n base in
          let rec aux tree fl =
            match tree with
            | Node (i, sum, left, right) -> 
                if i >= 0 then out.(i+1) <- f fl sum 
                else (
                  aux left fl;
                  match left with
                  | Node (_, x, _, _) -> aux right (f fl x)
                  | _                 -> raise (Failure "Unexpected condition.")
                )
            | Leaf -> ()
          in
          match tree with
          | Node (i, sum, left, right) ->
              if i >= 0 then out.(i+1) <- f fl sum
              else (
                match left with
                | Node (_, x, _, _) -> aux right (f fl x);
                | _                 -> raise (Failure "Unexpected condition.")
              );
              aux left fl; out
          | Leaf                       -> raise (Failure "Unexpected leaf.")
        in       
        let handler (ch: ('a result_message, 'a compute_message) Mpi.channel) () =
          let rec aux tree n () =
            match Mpi.receive ch with
            | CSum a ->
                let up = pps_up a in (
                  match up with
                  | Node (_, x, _, _) -> Mpi.send ch (RSum x)
                  | _                 -> raise (Failure "Unexpected condition.")
                );
                aux up (Array.length a) ()
            | Left l -> Mpi.send ch (Result (pps_down tree l (n+1)))
          in
          aux (Node (-1, base, Leaf, Leaf)) 0 ()
        in
        let len = chunk_n @@ Array.length @@ seq in
        let chunks = Array.make len [||] in
        split_array seq len 0 chunks;
        let channels = Array.map (fun _ -> Mpi.spawn handler ()) chunks in
        Array.iteri (fun i x -> Mpi.send x (CSum chunks.(i))) channels;
        let middle = Array.map (fun elt -> base) chunks in
        Array.iteri (fun i x -> let RSum s = Mpi.receive x in 
                                middle.(i) <- s) channels;
        let root = pps_up middle in
        let fl = (pps_down root base (len+1)) in
        Array.iteri (fun i x -> Mpi.send x (Left fl.(i))) channels;
        Array.iteri (fun i x -> let Result c = Mpi.receive x in
                                chunks.(i) <- c) channels;
        Array.iter (fun x -> Mpi.wait_die x) channels;
        flatten (Array.map (fun x -> Array.sub x 1 (Array.length x - 1)) chunks)
    )
end
