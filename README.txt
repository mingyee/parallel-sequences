Partner 1's name: Alex Joo
Partner 1's login: ajoo

(Leave blank if you are working alone)
Partner 2's name: Ming-Yee Tsang
Partner 2's login: mtsang



What kind of machine did you use to run your experiments? 
Give the processor, number of cores, if you are using a VM.
-----------------------------------------------------------
Personal laptop Windows 7
Intel Pentium B950 (2.10 GHz, 2 cores)



Comment *briefly* on the chunking procedure in your sequence 
implementation. What did you find effective? What did you 
find ineffective? How many chunks did you use?
----------------------------------------------------
Split array into 3 * num_cores chunks, up to chunks 
as small as eight. 
Looked like fewer chunks helped scan





Comments, Problems and Design Decisions:
----------------------------------------






Suggestions for the Future or Random Comments:
----------------------------------------------


